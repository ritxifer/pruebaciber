var express = require('express');
var cors = require('cors');

var indexRouter = require('./routes/index');

var app = express();

app.use(cors());

//prueba

app.use('/', indexRouter);

app.get('/', (req, res) => {
  return res.send('API Credit Cards 1.0: Maristak Ikastetxea Durango');
});

//cambio para probar

module.exports = app;
